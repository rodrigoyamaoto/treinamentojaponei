package br.com.hst.treinamentohst.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import br.com.hst.treinamentohst.R;
import br.com.hst.treinamentohst.adapter.NamesAdapter;
import br.com.hst.treinamentohst.models.Student;

public class MainActivity extends AppCompatActivity
{
    public static final String CURRENT_NAME_BUTTON = "CURRENT_NAME_BUTTON";
    private static final int REQUEST_NEW_BUTTON_NAME = 6978;
    private Button mBotaoDesafio;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBotaoDesafio = (Button) findViewById(R.id.activity_main_bt_desafio);
        mBotaoDesafio.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(MainActivity.this, FormNameButtonActivity.class);
                intent.putExtra(CURRENT_NAME_BUTTON, mBotaoDesafio.getText().toString());

                startActivityForResult(intent, REQUEST_NEW_BUTTON_NAME);
            }
        });


        final List<Student> listStudent = Arrays.asList(
                new Student("Douglas", 26, getResources().getDrawable(R.drawable.douglas)),
                new Student("Rodrigo", 23, getResources().getDrawable(R.drawable.rodrigo)),
                new Student("Paulo", 2, getResources().getDrawable(R.drawable.douglas)),
                new Student("Leticia", 45, getResources().getDrawable(R.drawable.rodrigo))
        );

        ListView listView = (ListView) findViewById(R.id.activity_main_lv_names);
        listView.setAdapter(new NamesAdapter(this, listStudent));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
            {
                Toast.makeText(MainActivity.this, listStudent.get(position).getName(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode)
        {
            case REQUEST_NEW_BUTTON_NAME:
                if (resultCode == RESULT_OK)
                {
                    if (data.getExtras() != null)
                    {
                        String newName = data.getExtras().getString(FormNameButtonActivity.CURRENT_NEW_NAME_BUTTON);
                        mBotaoDesafio.setText(newName);
                    }
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }


    }
}
