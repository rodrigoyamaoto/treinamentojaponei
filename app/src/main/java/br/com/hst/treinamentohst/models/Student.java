package br.com.hst.treinamentohst.models;

import android.graphics.drawable.Drawable;

public class Student
{
    private final String name;
    private final int age;
    private final Drawable photo;

    public Student(String name, int age, Drawable photo)
    {
        this.name = name;
        this.age = age;
        this.photo = photo;
    }

    public String getName()
    {
        return name;
    }

    public int getAge()
    {
        return age;
    }

    public Drawable getPhoto()
    {
        return photo;
    }
}
