package br.com.hst.treinamentohst.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.com.hst.treinamentohst.R;

public class FormNameButtonActivity extends AppCompatActivity
{

    public static final String CURRENT_NEW_NAME_BUTTON = "CURRENT_NEW_NAME_BUTTON";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_name_button);

        final EditText etNewButtonName = (EditText) findViewById(R.id.et);
        Button buttonFinalize = (Button) findViewById(R.id.bt_finalize);

        Bundle extras = getIntent().getExtras();
        if(extras != null)
        {
            etNewButtonName.setText(extras.getString(MainActivity.CURRENT_NAME_BUTTON));
        }

        buttonFinalize.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String newButtonName = etNewButtonName.getText().toString();
                Intent resultIntent = new Intent();
                resultIntent.putExtra(CURRENT_NEW_NAME_BUTTON, newButtonName);
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });
    }
}
