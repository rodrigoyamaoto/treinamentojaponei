package br.com.hst.treinamentohst.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.hst.treinamentohst.R;
import br.com.hst.treinamentohst.models.Student;

/**
 * Created by douglas_martins on 31/07/2017.
 */

public class NamesAdapter extends BaseAdapter
{
    private final Context mContext;
    List<Student> mListStudent;

    public NamesAdapter(Context context, List<Student> list)
    {
        this.mListStudent = list;
        this.mContext = context;
    }

    @Override
    public int getCount()
    {
        return mListStudent.size();
    }

    @Override
    public Object getItem(int i)
    {
        return mListStudent.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return 0;
    }

    @Override
    public View getView(int position, View viewParent, ViewGroup viewGroup)
    {

        Student student = mListStudent.get(position);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_list_name, viewGroup, false);

        TextView name = view.findViewById(R.id.item_list_name_tv_name);
        TextView age = view.findViewById(R.id.item_list_name_tv_age);
        ImageView photo = view.findViewById(R.id.item_list_name_iv_photo);

        name.setText(student.getName());
        age.setText(String.valueOf(student.getAge()));
        photo.setImageDrawable(student.getPhoto());
        return view;
    }
}
